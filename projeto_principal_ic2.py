# importaçoes

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from sklearn import neighbors
from sklearn import svm


# função para extrair uma coluna específica de um array
def column(matrix, i):
    return [row[i] for row in matrix]


def plot_graph_data_only(X, y, plot=False):
    # define tamanho da imagem
    plt.figure(figsize=((12, 8)))

    # plota dados do CSV
    plt.scatter(column(X, 0), column(X, 1), c=y, s=2, cmap='brg', label=y)

    if (plot==True):
        plt.show()


def plot_svm_decision(X, y, kernel='rbf', gamma=1, k_folds=5, decisao=True):
    clf = SVC(kernel=kernel, gamma=gamma)
    # clf.fit(X, y)

    X_folds = np.array_split(X, k_folds)
    y_folds = np.array_split(y, k_folds)

    scores = list()
    for k in range(k_folds):
        X_train = list(X_folds)
        X_test = X_train.pop(k)

        X_train = np.concatenate(X_train)
        y_train = list(y_folds)
        y_test = y_train.pop(k)
        y_train = np.concatenate(y_train)

        print(clf.fit(X_train, y_train).score(X_test, y_test))

    score = clf.score(X, y)
    E_out = 1 - score

    print()
    print("score medio: ", score)
    print("e_out: ", E_out)

    plot_graph_data_only(X, y)

    ax = plt.gca()
    x = np.linspace(plt.xlim()[0], plt.xlim()[1], 50)
    y = np.linspace(plt.ylim()[0], plt.ylim()[1], 50)
    Y, X = np.meshgrid(y, x)
    P = np.zeros_like(X)

    for i, xi in enumerate(x):
        for j, yj in enumerate(y):
            P[i, j] = clf.decision_function(np.array([[xi, yj]]))

    # plota o classificador
    ax.contour(X, Y, P, colors='k',
               levels=[-1, 0, 1], alpha=0.5,
               linestyles=['-', '-', '-'])

    # marca os pontos de limite de decisão
    if (decisao == True):
        plt.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1], s=60, facecolors='none', edgecolors='k')

    # adiciona título no gráfico com dados de parâmetros
    ax.set(
        title="Kernel: {}, Gama: {}, K-fold: {}, Lim.Decisão: {} \nScore: {}, E_out: {}".format(kernel, gamma, k_folds,
                                                                                                decisao, score, E_out))

    plt.show()


def plot_knn_classification(n_neighbors=15):
    h = .02

    for weights in ['uniform', 'distance']:
        clf = neighbors.KNeighborsClassifier(n_neighbors, weights=weights)
        clf.fit(X, y)

        x_min, x_max = min(column(X, 0)) - 1, max(column(X, 0)) + 1
        y_min, y_max = min(column(X, 1)) - 1, max(column(X, 1)) + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))
        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])

        Z = Z.reshape(xx.shape)
        plt.figure()
        plt.pcolormesh(xx, yy, Z, cmap='brg')

        score = clf.score(X, y)
        E_out = 1 - score

        plt.scatter(column(X, 0), column(X, 1), c=y, cmap='brg', edgecolor='k', s=20)
        plt.xlim(xx.min(), xx.max())
        plt.ylim(yy.min(), yy.max())
        plt.title("KNN - Classificação em 3 classes \n k: %i, w: %s, score: %f, E_out: %f"
                  % (n_neighbors, weights, score, E_out))

        print()
        print("score medio: ", score)
        print("e_out: ", E_out)

        plt.show()


print("Iniciando execução: o arquivo banana.csv deve estar no caminho /datasets/banana.csv")

# importação do dataset banana
open_file = pd.read_csv("/datasets/banana.csv", sep=",")

# lendo colunas do arquivo CSV
at1 = open_file['At1']
at2 = open_file['At2']
y = open_file['Class']

# verifica a quantidade de linhas no arquivo CSV
n = len(open_file)

# definindo array para par de valores de at1 e at2 (x e y)
X = []

# copiando valores de x1 e x2 e criando array x
for i in range(n):
    X.append([at1[i], at2[i]])
    
    
    
    
    
    
    
    

########### execucao #############

# executando apenas o plot dos dados

plot_graph_data_only(X, y, plot=True)

# executando plot do SVM, de acordo com os paramestros:
#  kernel (sigmoid, linear, poly ou rbf)
#  gamma (1, 0.5 ou 0.01)
#  k_folds (2, 5 ou 10)
#  decisao (exibe ou não a margem de decisão)

plot_svm_decision(X, y, kernel='rbf', gamma=1, k_folds=10, decisao=True)

# executando plot do KNN
#  n_neighbors = número de vizinhos

plot_knn_classification(n_neighbors=15)



