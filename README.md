Trabalho prático de Inteligência Computacional 2

Implementação de SVM - Support Vector Machine


Execução:

#### IMPORTANTE: NECESSÁRIO O ARQUIVO BANANA.CSV NO CAMINHO /datasets/banana.csv



Executando apenas o plot dos dados:

```python
plot_graph_data_only(X, y, plot=True)
```


Executando plot do SVM, de acordo com os paramestros:
     kernel (sigmoid, linear, poly ou rbf)
     gamma (1, 0.5 ou 0.01)
     k_folds (2, 5 ou 10)
     decisao (exibe ou não a margem de decisão)

```python
plot_svm_decision(X, y, kernel='rbf', gamma=1, k_folds=10, decisao=True)
```


Executando plot do KNN
     n_neighbors = número de vizinhos

```python
plot_knn_classification(n_neighbors=15)
```